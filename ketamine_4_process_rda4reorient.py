import sys, os, glob, fnmatch, shutil,commands,re
import nipype.interfaces.fsl as fsl

#this script should only be applied with Ketamine data from 1_messung.
#for the 2_messung and 3_messung, it's not realiable

src_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess"
subjs = os.listdir(src_dir)
#subjs.remove("ac49")
subjs = ["bh27_2_Messung"]
subjs.sort()

for subj in subjs:
	src_subjfolder = os.path.join(src_dir,subj)
	if not subj.startswith('.') and os.path.isdir(src_subjfolder):
		print subj
		# subj_1_messung = subj.replace('_2_Messung','_1_Messung')
		# subj_1_messung = subj.replace('_3_Messung','_1_Messung')

		enMPRAGE = os.path.join(src_dir,subj[0:4] + "_1_Messung/T1/imcalc/enMPRAGE_corrected.nii")

		tmp_img = glob.glob(os.path.join(src_subjfolder,'T1/imcalc/MPRAGE/convert_BET/convert_dicom/20*.img'))
		tmp_nii = glob.glob(os.path.join(src_subjfolder,'T1/imcalc/MPRAGE/convert_BET/convert_dicom/20*.nii'))
		if len(tmp_img) == 0 and len(tmp_nii) == 1:
			chtype = ('/usr/local/fsl/bin/fslchfiletype NIFTI_PAIR %s %s'%(tmp_nii[0],tmp_nii[0].replace('.nii','.img')))
			#print chtype
			#os.system(chtype)		
			t1_mprage = tmp_nii[0].replace('.nii','.img')
			orig_nii = os.path.join(os.path.join(src_subjfolder,'T1/imcalc/MPRAGE/convert_BET/convert_dicom/orig_nii'))
			if not os.path.exists(orig_nii):			
				os.mkdir(orig_nii)
				cp_orig_nii = ('cp %s %s'%(tmp_nii[0],orig_nii))
				#print cp_orig_nii
				#os.system(cp_orig_nii)
		elif len(tmp_img) == 1 :
			t1_mprage = tmp_img[0]
		print t1_mprage		
		tmp = glob.glob(os.path.join(src_subjfolder,'MRS_AMCC/20*.img'))
		rda_amcc = os.path.abspath(tmp[0])
		print rda_amcc

		tmp = glob.glob(os.path.join(src_subjfolder,'MRS_PGACC/20*.img'))
		rda_pgacc = os.path.abspath(tmp[0])
		print rda_pgacc

		# t1_mprage_nii_bk = ('mv %s %s'%(t1_mprage_nii,orig_nii))
		# print t1_mprage_nii_bk
		# os.system(t1_mprage_nii_bk)

		# t1_mprage_img_bk = ('cp %s %s'%(t1_mprage,t1_mprage.replace('.img','_bk.img')))
		# rda_amcc_img_bk = ('cp %s %s'%(rda_amcc,rda_amcc.replace('.img','_bk.img')))
		# rda_pgacc_img_bk = ('cp %s %s'%(rda_pgacc,rda_pgacc.replace('.img','_bk.img')))

		# print t1_mprage_img_bk
		# print rda_amcc_img_bk		
		# print rda_pgacc_img_bk		
		# os.system(t1_mprage_img_bk)
		# os.system(rda_amcc_img_bk)
		# os.system(rda_pgacc_img_bk)

		# t1_mprage_hdr_bk = ('cp %s %s'%(t1_mprage,t1_mprage.replace('.hdr','_bk.hdr')))
		# rda_amcc_hdr_bk = ('cp %s %s'%(rda_amcc,rda_amcc.replace('.hdr','_bk.hdr')))
		# rda_pgacc_hdr_bk = ('cp %s %s'%(rda_pgacc,rda_pgacc.replace('.hdr','_bk.hdr')))

		# print t1_mprage_hdr_bk
		# print rda_amcc_hdr_bk		
		# print rda_pgacc_hdr_bk		
		# os.system(t1_mprage_hdr_bk)
		# os.system(rda_amcc_hdr_bk)
		# os.system(rda_pgacc_hdr_bk)

		# mrsfilter = ('%s/MRS*/*img'%(src_subjfolder)) 
		# mrsfiles = glob.glob(mrsfilter)
		# print mrsfiles

		# rda_img_path = ("%s/*.img"%(os.chdir(os.path.join(src_subjfolder,folder)))
		# rda_img = glob.glob(rda_folder_path)		


		ren_t1_mprage = os.path.dirname(t1_mprage) + '/ren_' + os.path.basename(t1_mprage)
		ren_rda_amcc = os.path.dirname(rda_amcc) + '/ren_' + os.path.basename(rda_amcc)
		ren_rda_pgacc = os.path.dirname(rda_pgacc) + '/ren_' + os.path.basename(rda_pgacc)

		# ants_rigid = ("ANTS 3 -m MI[%s,%s,1,32] -i 0 -o %s --rigid-affine true" %(enMPRAGE,t1_mprage,ren_t1_mprage))
		# ants_wrap_t1 = ("WarpImageMultiTransform 3 %s %s -R %s r_Affine.txt "%(t1_mprage,ren_t1_mprage,enMPRAGE))
		# ants_wrap_amcc = ("WarpImageMultiTransform 3 %s %s -R %s r_Affine.txt "%(rda_amcc,ren_rda_amcc,enMPRAGE))
		# ants_wrap_pgacc = ("WarpImageMultiTransform 3 %s %s -R %s r_Affine.txt "%(rda_pgacc,ren_rda_pgacc,enMPRAGE))

		# print ants_rigid
		# print ants_wrap_t1
		# print ants_wrap_amcc
		# print ants_wrap_pgacc

		# os.system(ants_rigid)
		# os.system(ants_t1)
		# os.system(ants_amcc)
		# os.system(ants_pgacc)

		os.chdir(os.path.join(src_subjfolder,'T1/imcalc/'))
		if not os.path.exists(ren_t1_mprage):
			print enMPRAGE
			print ren_t1_mprage
			flt = fsl.FLIRT(bins=640, cost_func='mutualinfo')
			flt.inputs.in_file = t1_mprage
			flt.inputs.reference = enMPRAGE
			flt.inputs.out_file = ren_t1_mprage
			flt.inputs.output_type = 'NIFTI_PAIR'
			flt.inputs.out_matrix_file = os.path.join(src_subjfolder,'T1/imcalc/MPRAGE/convert_BET/convert_dicom/mprage_to_enMPRAGE.mat')
			print flt.cmdline
			res = flt.run() 

		os.chdir(os.path.join(src_subjfolder,'MRS_AMCC'))
		if not os.path.exists(ren_rda_amcc):
			print ren_rda_amcc
			amcc_applyxfm = fsl.ApplyXfm()
			amcc_applyxfm.inputs.in_file = rda_amcc
			amcc_applyxfm.inputs.in_matrix_file = os.path.join(src_subjfolder,'T1/imcalc/MPRAGE/convert_BET/convert_dicom/mprage_to_enMPRAGE.mat')
			amcc_applyxfm.inputs.out_file = ren_rda_amcc
			amcc_applyxfm.inputs.reference = enMPRAGE
			amcc_applyxfm.inputs.output_type = 'NIFTI_PAIR'
			amcc_applyxfm.inputs.apply_xfm = True
			print amcc_applyxfm.cmdline
			amcc_result = amcc_applyxfm.run() 

		os.chdir(os.path.join(src_subjfolder,'MRS_PGACC'))
		if not os.path.exists(ren_rda_pgacc):
			print ren_rda_pgacc
			pgacc_applyxfm = fsl.ApplyXfm()
			pgacc_applyxfm.inputs.in_file = rda_pgacc
			pgacc_applyxfm.inputs.in_matrix_file = os.path.join(src_subjfolder,'T1/imcalc/MPRAGE/convert_BET/convert_dicom/mprage_to_enMPRAGE.mat')
			pgacc_applyxfm.inputs.out_file = ren_rda_pgacc
			pgacc_applyxfm.inputs.output_type = 'NIFTI_PAIR'
			pgacc_applyxfm.inputs.reference = enMPRAGE
			pgacc_applyxfm.inputs.apply_xfm = True
			print pgacc_applyxfm
			pgacc_result = pgacc_applyxfm.run() 

		# coregister = spm.Coregister()
		# coregister.inputs.jobtype = 'write'
		# coregister.inputs.target = '/Volumes/Data_Meng_Canlab/mni_icbm152_nlin_sym_09b_nifti/mni_icbm152_nlin_sym_09b_Resliced/Resliced_mni_icbm152_t1_tal_nlin_sym_09b_hires.nii'
		# coregister.inputs.source = mpragefile[0:-3]
		# coregister.inputs.apply_to_files = mrsfiles
		# coregister.run()




