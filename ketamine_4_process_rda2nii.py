import sys, os, glob, fnmatch, shutil,commands
import nipype.interfaces.matlab as matlab
import nipype.interfaces.spm as spm          # spm

mlab = matlab.MatlabCommand()

src_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess"
#src_dir = "/Volumes/Data_Meng_Canlab/Weiqiang-preprocess"
os.chdir(src_dir)
#subjids = glob.glob("*_1_Messung")
subjids = os.listdir(src_dir)
subjids.sort()

#subjids.remove("bh27_1_Messung")
#subjids.remove("bh27_2_Messung")
#subjids.remove('.DS_Store')
#subjs.remove("ac49")
#subjs = ["ac49"]
# dst = "/Volumes/Data_Meng_Canlab/Weiqiang-preprocess/"
#dst = src


for subjid in subjids:
	print subjid
	if not subjid.startswith("."):
		src_subjfolder = os.path.join(src_dir,subjid)
		folders = os.listdir(src_subjfolder)

		t1image_path_img = os.path.join(src_subjfolder,"T1/imcalc/MPRAGE/convert_BET/convert_dicom/20*.img")
		t1image_path_nii = os.path.join(src_subjfolder,"T1/imcalc/MPRAGE/convert_BET/convert_dicom/20*.nii")
		t1image_img = glob.glob(t1image_path_img)
		t1image_nii = glob.glob(t1image_path_nii)
		if len(t1image_img) == 1:
			t1folder = os.path.dirname(t1image_img[0])
			t1image = os.path.basename(t1image_img[0])
			t1image_pair = t1image
		elif len(t1image_img) == 0 and len(t1image_nii) == 1:
			t1folder = os.path.dirname(t1image_nii[0])
			t1image = os.path.basename(t1image_nii[0])
			chtype = ('/usr/local/fsl/bin/fslchfiletype NIFTI_PAIR %s %s'%(t1image_nii[0],t1image_nii[0].replace('.nii','.img')))
			print chtype
			#os.system(chtype)
			t1image_pair = t1image.replace('.nii','.img')
		print t1folder
		print t1image

		for folder in folders:
			rdaimage_path_temp = ("%s/*.rda"%(os.path.join(src_subjfolder,folder)))
			rdaimages = glob.glob(rdaimage_path_temp)
			rda2nii = glob.glob(rdaimage_path_temp.replace('rda','hdr'))

			if len(rdaimages)==2 and len(rda2nii) == 0:
				print rdaimages

				for rdaimage in rdaimages:
					if not "_h2" in rdaimage.lower(): 
						rdafolder = os.path.dirname(rdaimages[0])
						rdaimage = os.path.basename(rdaimages[0])	
						os.chdir(rdafolder) 
						svs2mask = ("addpath(genpath('/Users/canlab06/Documents/Soft/matlab_addon/mrs_tools'));svs2mask_modified_meng(\'%s/\',\'%s\',\'%s/\',\'%s\')"%(t1folder,t1image_pair,rdafolder,rdaimage))
						print svs2mask
						mlab.inputs.script = svs2mask
						#out = mlab.run()