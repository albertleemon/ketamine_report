clear
clc

% workingfolder = '/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess';
workingfolder = '/Volumes/Data_Meng_Canlab/Weiqiang-preprocess';
subjects = dir(workingfolder);
%ind2rm = false(1,numel(subjects));
ind2rm = [];
for iter = 1: numel(subjects)
    if subjects(iter).isdir == 0 | strfind(subjects(iter).name,'.')
        ind2rm(numel(ind2rm)+1) = iter;
    end
end
subjects(ind2rm) = [];
subjnumber = numel(subjects);

gm_wm_csf = zeros(subjnumber,16);
 
%% only for ketamine 
%for crun = 1:subjnumber
%     fprintf('%s\n',subjects(crun).name);
%     rdafolder = fullfile(workingfolder,subjects(crun).name);
%     
% % only for Ketamine
%     rdaamcc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_AMCC'),'^ren_.*\.img$');
%     rdapgacc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_PGACC'),'^ren_.*\.img$');
% %     rda23c = spm_select('ExtFPList',fullfile(rdafolder,'MRS_23C'),'^ren20.*\.img$');
% %     rdapcc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_PCC'),'^ren20.*\.img$');
%     
%     fprintf('%s\n',rdaamcc);
%     fprintf('%s\n',rdapgacc);
% %     fprintf('%s\n',rda23c);
% %     fprintf('%s\n',rdapcc);
%     subj = subjects(crun).name(1:4); 
%     t1folder = fullfile(workingfolder,[subj,'_1_Messung'],'T1','imcalc');
% 
% 
% 
%     p1image = fullfile(t1folder,'p1enMPRAGE_corrected.nii');
%     p2image = fullfile(t1folder,'p2enMPRAGE_corrected.nii');
%     p3image = fullfile(t1folder,'p3enMPRAGE_corrected.nii');
%     
%     %     if exist(p1image) == 0
%     %         p1image = fullfile(t1folder,'sp1nenMPRAGE.nii');
%     %         p2image = fullfile(t1folder,'sp2nenMPRAGE.nii');
%     %         p3image = fullfile(t1folder,'sp3nenMPRAGE.nii');
%     %     end
%     
%     fprintf('%s\n',p1image);
%     fprintf('%s\n',p2image);
%     fprintf('%s\n',p3image);
%     
%     p1HeaderInfo = spm_vol(p1image);
%     p1Data = spm_read_vols(p1HeaderInfo);
%     p2HeaderInfo = spm_vol(p2image);
%     p2Data = spm_read_vols(p2HeaderInfo);
%     p3HeaderInfo = spm_vol(p3image);
%     p3Data = spm_read_vols(p3HeaderInfo);
%     
%     if numel(rdaamcc) ~= 0
%         rdaamccHeaderInfo = spm_vol(rdaamcc);
%         rdaamccData = spm_read_vols(rdaamccHeaderInfo);
%         rdaamccData(rdaamccData>0) = 1;
%         temp = p1Data.* rdaamccData;
%         gm_wm_csf(crun,1) = sum(temp(:));
%         temp = p2Data.* rdaamccData;
%         gm_wm_csf(crun,2) = sum(temp(:));
%         temp = p3Data.* rdaamccData;
%         gm_wm_csf(crun,3) = sum(temp(:));
%         
%         %         temp = (rdaamccData>0 & (p1Data>0));
%         %         gm_wm_csf(crun,1) = numel(find(temp==1));
%         %         temp = (rdaamccData>0 & (p2Data>0));
%         %         gm_wm_csf(crun,2) = numel(find(temp==1));
%         %         temp = (rdaamccData>0 & (p3Data>0));
%         %         gm_wm_csf(crun,3) = numel(find(temp==1));
%     else
%         continue
%     end
%     
%     if numel(rdapgacc) ~= 0
%         rdapgaccHeaderInfo = spm_vol(rdapgacc);
%         rdapgaccData = spm_read_vols(rdapgaccHeaderInfo);
%         rdapgaccData(rdapgaccData>0) = 1;
%         temp = p1Data.* rdapgaccData;
%         gm_wm_csf(crun,4) = sum(temp(:));
%         temp = p2Data.* rdapgaccData;
%         gm_wm_csf(crun,5) = sum(temp(:));
%         temp = p3Data.* rdapgaccData;
%         gm_wm_csf(crun,6) = sum(temp(:));
%         
%         %         temp = (rdapgaccData>0 & (p1Data>0.2));
%         %         gm_wm_csf(crun,4) = numel(find(temp==1));
%         %         temp = (rdapgaccData>0 & (p2Data>0.2));
%         %         gm_wm_csf(crun,5) = numel(find(temp==1));
%         %         temp = (rdapgaccData>0 & (p3Data>0.2));
%         %         gm_wm_csf(crun,6) = numel(find(temp==1));
%     else
%         continue
%     end
%     
% %     
% %     if numel(rda23c) ~= 0
% %         rda23cHeaderInfo = spm_vol(rda23c);
% %         rda23cData = spm_read_vols(rda23cHeaderInfo);
% %         rda23cData(rda23cData>0) = 1;
% %         temp = p1Data.* rda23cData;
% %         gm_wm_csf(crun,7) = sum(temp(:));
% %         temp = p2Data.* rda23cData;
% %         gm_wm_csf(crun,8) = sum(temp(:));
% %         temp = p3Data.* rda23cData;
% %         gm_wm_csf(crun,9) = sum(temp(:));
% %         
% %         %         temp = (rda23cData>0 & (p1Data>0));
% %         %         gm_wm_csf(crun,1) = numel(find(temp==1));
% %         %         temp = (rda23cData>0 & (p2Data>0));
% %         %         gm_wm_csf(crun,2) = numel(find(temp==1));
% %         %         temp = (rda23cData>0 & (p3Data>0));
% %         %         gm_wm_csf(crun,3) = numel(find(temp==1));
% %     else
% %         continue
% %     end
% %     
% %     if numel(rdapcc) ~= 0
% %         rdapccHeaderInfo = spm_vol(rdapcc);
% %         rdapccData = spm_read_vols(rdapccHeaderInfo);
% %         rdapccData(rdapccData>0) = 1;
% %         temp = p1Data.* rdapccData;
% %         gm_wm_csf(crun,10) = sum(temp(:));
% %         temp = p2Data.* rdapccData;
% %         gm_wm_csf(crun,11) = sum(temp(:));
% %         temp = p3Data.* rdapccData;
% %         gm_wm_csf(crun,12) = sum(temp(:));
% %         
% %         %         temp = (rdapccData>0 & (p1Data>0.2));
% %         %         gm_wm_csf(crun,4) = numel(find(temp==1));
% %         %         temp = (rdapccData>0 & (p2Data>0.2));
% %         %         gm_wm_csf(crun,5) = numel(find(temp==1));
% %         %         temp = (rdapccData>0 & (p3Data>0.2));
% %         %         gm_wm_csf(crun,6) = numel(find(temp==1));
% %     else
% %         continue
% %     end
%     
% end
% amcc = gm_wm_csf(:,1:3);
% pgacc = gm_wm_csf(:,4:6);
% % c23 = gm_wm_csf(:,7:9);
% % pcc = gm_wm_csf(:,10:12);
% gm_wm_csf(:,17) = sum(amcc,2);
% gm_wm_csf(:,18) = sum(pgacc,2);
% % gm_wm_csf(:,19) = sum(c23,2);
% % gm_wm_csf(:,20) = sum(pcc,2);
% 
% 
% ratio_amcc= amcc(:,1)./sum(amcc,2);
% ratio_pgacc = pgacc(:,1)./sum(pgacc,2);
% % ratio_23c= amcc(:,1)./sum(c23,2);
% % ratio_pcc = pgacc(:,1)./sum(pcc,2);
% 
% gm_wm_csf(:,13) = ratio_amcc;
% gm_wm_csf(:,14) = ratio_pgacc;
% % gm_wm_csf(:,15) = ratio_23c;
% % gm_wm_csf(:,16) = ratio_pcc;
%% only for Weiqiang
for crun = 1:subjnumber
    fprintf('%s\n',subjects(crun).name);
    rdafolder = fullfile(workingfolder,subjects(crun).name);
    rdaamcc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_AMCC'),'^20.*\.img$');
    rdapgacc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_PGACC'),'^20.*\.img$');
    rda23c = spm_select('ExtFPList',fullfile(rdafolder,'MRS_23C'),'^20.*\.img$');
    rdapcc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_PCC'),'^20.*\.img$');
    
    fprintf('%s\n',rdaamcc);
    fprintf('%s\n',rdapgacc);
    fprintf('%s\n',rda23c);
    fprintf('%s\n',rdapcc);
    t1folder = fullfile(workingfolder,subjects(crun).name,'T1','imcalc');

    p1image = fullfile(t1folder,'p1enMPRAGE_corrected.nii');
    p2image = fullfile(t1folder,'p2enMPRAGE_corrected.nii');
    p3image = fullfile(t1folder,'p3enMPRAGE_corrected.nii');
    
    %     if exist(p1image) == 0
    %         p1image = fullfile(t1folder,'sp1nenMPRAGE.nii');
    %         p2image = fullfile(t1folder,'sp2nenMPRAGE.nii');
    %         p3image = fullfile(t1folder,'sp3nenMPRAGE.nii');
    %     end
    
    fprintf('%s\n',p1image);
    fprintf('%s\n',p2image);
    fprintf('%s\n',p3image);
    
    p1HeaderInfo = spm_vol(p1image);
    p1Data = spm_read_vols(p1HeaderInfo);
    p2HeaderInfo = spm_vol(p2image);
    p2Data = spm_read_vols(p2HeaderInfo);
    p3HeaderInfo = spm_vol(p3image);
    p3Data = spm_read_vols(p3HeaderInfo);
    
    if numel(rdaamcc) ~= 0
        rdaamccHeaderInfo = spm_vol(rdaamcc);
        rdaamccData = spm_read_vols(rdaamccHeaderInfo);
        rdaamccData(rdaamccData>0) = 1;
        temp = p1Data.* rdaamccData;
        gm_wm_csf(crun,1) = sum(temp(:));
        temp = p2Data.* rdaamccData;
        gm_wm_csf(crun,2) = sum(temp(:));
        temp = p3Data.* rdaamccData;
        gm_wm_csf(crun,3) = sum(temp(:));
        
        %         temp = (rdaamccData>0 & (p1Data>0));
        %         gm_wm_csf(crun,1) = numel(find(temp==1));
        %         temp = (rdaamccData>0 & (p2Data>0));
        %         gm_wm_csf(crun,2) = numel(find(temp==1));
        %         temp = (rdaamccData>0 & (p3Data>0));
        %         gm_wm_csf(crun,3) = numel(find(temp==1));
    else
        continue
    end
    
    if numel(rdapgacc) ~= 0
        rdapgaccHeaderInfo = spm_vol(rdapgacc);
        rdapgaccData = spm_read_vols(rdapgaccHeaderInfo);
        rdapgaccData(rdapgaccData>0) = 1;
        temp = p1Data.* rdapgaccData;
        gm_wm_csf(crun,4) = sum(temp(:));
        temp = p2Data.* rdapgaccData;
        gm_wm_csf(crun,5) = sum(temp(:));
        temp = p3Data.* rdapgaccData;
        gm_wm_csf(crun,6) = sum(temp(:));
        
        %         temp = (rdapgaccData>0 & (p1Data>0.2));
        %         gm_wm_csf(crun,4) = numel(find(temp==1));
        %         temp = (rdapgaccData>0 & (p2Data>0.2));
        %         gm_wm_csf(crun,5) = numel(find(temp==1));
        %         temp = (rdapgaccData>0 & (p3Data>0.2));
        %         gm_wm_csf(crun,6) = numel(find(temp==1));
    else
        continue
    end
    
    
    if numel(rda23c) ~= 0
        rda23cHeaderInfo = spm_vol(rda23c);
        rda23cData = spm_read_vols(rda23cHeaderInfo);
        rda23cData(rda23cData>0) = 1;
        temp = p1Data.* rda23cData;
        gm_wm_csf(crun,7) = sum(temp(:));
        temp = p2Data.* rda23cData;
        gm_wm_csf(crun,8) = sum(temp(:));
        temp = p3Data.* rda23cData;
        gm_wm_csf(crun,9) = sum(temp(:));
        
        %         temp = (rda23cData>0 & (p1Data>0));
        %         gm_wm_csf(crun,1) = numel(find(temp==1));
        %         temp = (rda23cData>0 & (p2Data>0));
        %         gm_wm_csf(crun,2) = numel(find(temp==1));
        %         temp = (rda23cData>0 & (p3Data>0));
        %         gm_wm_csf(crun,3) = numel(find(temp==1));
    else
        continue
    end
    
    if numel(rdapcc) ~= 0
        rdapccHeaderInfo = spm_vol(rdapcc);
        rdapccData = spm_read_vols(rdapccHeaderInfo);
        rdapccData(rdapccData>0) = 1;
        temp = p1Data.* rdapccData;
        gm_wm_csf(crun,10) = sum(temp(:));
        temp = p2Data.* rdapccData;
        gm_wm_csf(crun,11) = sum(temp(:));
        temp = p3Data.* rdapccData;
        gm_wm_csf(crun,12) = sum(temp(:));
        
        %         temp = (rdapccData>0 & (p1Data>0.2));
        %         gm_wm_csf(crun,4) = numel(find(temp==1));
        %         temp = (rdapccData>0 & (p2Data>0.2));
        %         gm_wm_csf(crun,5) = numel(find(temp==1));
        %         temp = (rdapccData>0 & (p3Data>0.2));
        %         gm_wm_csf(crun,6) = numel(find(temp==1));
    else
        continue
    end
    
end

amcc = gm_wm_csf(:,1:3);
pgacc = gm_wm_csf(:,4:6);
c23 = gm_wm_csf(:,7:9);
pcc = gm_wm_csf(:,10:12);
gm_wm_csf(:,17) = sum(amcc,2);
gm_wm_csf(:,18) = sum(pgacc,2);
gm_wm_csf(:,19) = sum(c23,2);
gm_wm_csf(:,20) = sum(pcc,2);


ratio_amcc= amcc(:,1)./sum(amcc,2);
ratio_pgacc = pgacc(:,1)./sum(pgacc,2);
ratio_23c= amcc(:,1)./sum(c23,2);
ratio_pcc = pgacc(:,1)./sum(pcc,2);

gm_wm_csf(:,13) = ratio_amcc;
gm_wm_csf(:,14) = ratio_pgacc;
gm_wm_csf(:,15) = ratio_23c;
gm_wm_csf(:,16) = ratio_pcc;