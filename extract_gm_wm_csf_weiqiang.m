clear
clc

workingfolder = '/Volumes/Data_Meng_Canlab/Weiqiang-preprocess';
subjects = dir(workingfolder);
%ind2rm = false(1,numel(subjects));
ind2rm = [];
for iter = 1: numel(subjects)
    if subjects(iter).isdir == 0 | strfind(subjects(iter).name,'.')
        ind2rm(numel(ind2rm)+1) = iter;
    end
end
subjects(ind2rm) = [];
subjnumber = numel(subjects);

gm_wm_csf = zeros(subjnumber,16);

for crun = id
    fprintf('%s\n',subjects(crun).name);
    rdafolder = fullfile(workingfolder,subjects(crun).name);
    rdaamcc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_AMCC'),'^20.*\.img$');
    rdapgacc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_PGACC'),'^20.*\.img$');
    rda23c = spm_select('ExtFPList',fullfile(rdafolder,'MRS_23C'),'^20.*\.img$');
    rdapcc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_PCC'),'^20.*\.img$');
    
    fprintf('%s\n',rdaamcc);
    fprintf('%s\n',rdapgacc);
    fprintf('%s\n',rda23c);
    fprintf('%s\n',rdapcc);
    
    
    t1folder = fullfile(workingfolder,subjects(crun).name,'T1','imcalc');
    p1image = fullfile(t1folder,'p1enMPRAGE_corrected.nii');
    p2image = fullfile(t1folder,'p2enMPRAGE_corrected.nii');
    p3image = fullfile(t1folder,'p3enMPRAGE_corrected.nii');
    
    %     if exist(p1image) == 0
    %         p1image = fullfile(t1folder,'sp1nenMPRAGE.nii');
    %         p2image = fullfile(t1folder,'sp2nenMPRAGE.nii');
    %         p3image = fullfile(t1folder,'sp3nenMPRAGE.nii');
    %     end
    
    fprintf('%s\n',p1image);
    fprintf('%s\n',p2image);
    fprintf('%s\n',p3image);
    
    p1HeaderInfo = spm_vol(p1image);
    p1Data = spm_read_vols(p1HeaderInfo);
    p2HeaderInfo = spm_vol(p2image);
    p2Data = spm_read_vols(p2HeaderInfo);
    p3HeaderInfo = spm_vol(p3image);
    p3Data = spm_read_vols(p3HeaderInfo);
    
    if numel(rdaamcc) ~= 0
        rdaamccHeaderInfo = spm_vol(rdaamcc);
        rdaamccData = spm_read_vols(rdaamccHeaderInfo);
        rdaamccData(rdaamccData>0) = 1;
        temp = p1Data.* rdaamccData;
        gm_wm_csf(crun,1) = sum(temp(:));
        temp = p2Data.* rdaamccData;
        gm_wm_csf(crun,2) = sum(temp(:));
        temp = p3Data.* rdaamccData;
        gm_wm_csf(crun,3) = sum(temp(:));
        
        %         temp = (rdaamccData>0 & (p1Data>0));
        %         gm_wm_csf(crun,1) = numel(find(temp==1));
        %         temp = (rdaamccData>0 & (p2Data>0));
        %         gm_wm_csf(crun,2) = numel(find(temp==1));
        %         temp = (rdaamccData>0 & (p3Data>0));
        %         gm_wm_csf(crun,3) = numel(find(temp==1));
    else
        continue
    end
    
    if numel(rdapgacc) ~= 0
        rdapgaccHeaderInfo = spm_vol(rdapgacc);
        rdapgaccData = spm_read_vols(rdapgaccHeaderInfo);
        rdapgaccData(rdapgaccData>0) = 1;
        temp = p1Data.* rdapgaccData;
        gm_wm_csf(crun,4) = sum(temp(:));
        temp = p2Data.* rdapgaccData;
        gm_wm_csf(crun,5) = sum(temp(:));
        temp = p3Data.* rdapgaccData;
        gm_wm_csf(crun,6) = sum(temp(:));
        
        %         temp = (rdapgaccData>0 & (p1Data>0.2));
        %         gm_wm_csf(crun,4) = numel(find(temp==1));
        %         temp = (rdapgaccData>0 & (p2Data>0.2));
        %         gm_wm_csf(crun,5) = numel(find(temp==1));
        %         temp = (rdapgaccData>0 & (p3Data>0.2));
        %         gm_wm_csf(crun,6) = numel(find(temp==1));
    else
        continue
    end
    
    
    if numel(rda23c) ~= 0
        rda23cHeaderInfo = spm_vol(rda23c);
        rda23cData = spm_read_vols(rda23cHeaderInfo);
        rda23cData(rda23cData>0) = 1;
        temp = p1Data.* rda23cData;
        gm_wm_csf(crun,7) = sum(temp(:));
        temp = p2Data.* rda23cData;
        gm_wm_csf(crun,8) = sum(temp(:));
        temp = p3Data.* rda23cData;
        gm_wm_csf(crun,9) = sum(temp(:));
        
        %         temp = (rda23cData>0 & (p1Data>0));
        %         gm_wm_csf(crun,1) = numel(find(temp==1));
        %         temp = (rda23cData>0 & (p2Data>0));
        %         gm_wm_csf(crun,2) = numel(find(temp==1));
        %         temp = (rda23cData>0 & (p3Data>0));
        %         gm_wm_csf(crun,3) = numel(find(temp==1));
    else
        continue
    end
    
    if numel(rdapcc) ~= 0
        rdapccHeaderInfo = spm_vol(rdapcc);
        rdapccData = spm_read_vols(rdapccHeaderInfo);
        rdapccData(rdapccData>0) = 1;
        temp = p1Data.* rdapccData;
        gm_wm_csf(crun,10) = sum(temp(:));
        temp = p2Data.* rdapccData;
        gm_wm_csf(crun,11) = sum(temp(:));
        temp = p3Data.* rdapccData;
        gm_wm_csf(crun,12) = sum(temp(:));
        
        %         temp = (rdapccData>0 & (p1Data>0.2));
        %         gm_wm_csf(crun,4) = numel(find(temp==1));
        %         temp = (rdapccData>0 & (p2Data>0.2));
        %         gm_wm_csf(crun,5) = numel(find(temp==1));
        %         temp = (rdapccData>0 & (p3Data>0.2));
        %         gm_wm_csf(crun,6) = numel(find(temp==1));
    else
        continue
    end
    
end

amcc = gm_wm_csf(:,1:3);
pgacc = gm_wm_csf(:,4:6);
c23 = gm_wm_csf(:,7:9);
pcc = gm_wm_csf(:,10:12);
gm_wm_csf(:,17) = sum(amcc,2);
gm_wm_csf(:,18) = sum(pgacc,2);
gm_wm_csf(:,19) = sum(c23,2);
gm_wm_csf(:,20) = sum(pcc,2);


ratio_amcc= amcc(:,1)./sum(amcc,2);
ratio_pgacc = pgacc(:,1)./sum(pgacc,2);
ratio_23c= amcc(:,1)./sum(c23,2);
ratio_pcc = pgacc(:,1)./sum(pcc,2);

gm_wm_csf(:,13) = ratio_amcc;
gm_wm_csf(:,14) = ratio_pgacc;
gm_wm_csf(:,15) = ratio_23c;
gm_wm_csf(:,16) = ratio_pcc;


























crun = 11
crun = 48;
crun = 85;
enimage = spm_select('ExtFPList',fullfile(workingfolder,[num2str(1),subjects(crun).name(2:end)]),'^enMPRAGE.nii$');
enhead = spm_vol(enimage);

templateimage = spm_select('ExtFPList',fullfile(workingfolder,[num2str(1),subjects(crun).name(2:end)]),'^nenMPRAGE.nii$');
nenhead = spm_vol(templateimage);

t1folder  = fullfile(workingfolder,subjects(crun).name,'MPRAGE','convert_BET','convert_dicom');
t1image = spm_select('ExtFPList',t1folder,'^r201.*\.nii$');
t1head = spm_vol(t1image);

amcc_rdafolder = fullfile(workingfolder,subjects(crun).name,'RDA','AMCC');
amcc_rdafiles = spm_select('ExtFPList',amcc_rdafolder,'^r201.*\.img$');
amcchead = spm_vol(amcc_rdafiles);
pgacc_rdafolder = fullfile(workingfolder,subjects(crun).name,'RDA','PGACC');
pgacc_rdafiles = spm_select('ExtFPList',pgacc_rdafolder,'^r201.*\.img$');
pgacchead = spm_vol(pgacc_rdafiles);

fprintf('%s\n',enimage);
enhead.mat
fprintf('%s\n',templateimage);
nenhead.mat
fprintf('%s\n',t1image);
t1head.mat
fprintf('%s\n',amcc_rdafiles);
amcchead.mat
fprintf('%s\n',pgacc_rdafiles);
pgacchead.mat

nendata = spm_read_vols(nenhead);
nenhead.mat = enhead.mat;
spm_write_vol(nenhead,nendata);

t1data = spm_read_vols(t1head);
t1head.mat = enhead.mat;
spm_write_vol(t1head,t1data);

amccdata = spm_read_vols(amcchead);
amcchead.mat = enhead.mat;
spm_write_vol(amcchead,amccdata);

pgaccdata = spm_read_vols(pgacchead);
pgacchead.mat = enhead.mat;
spm_write_vol(pgacchead,pgaccdata);

gm_wm_csf = zeros(subjnumber,8);
for crun = 1: subjnumber
    fprintf('%s\n',subjects(crun).name);
    
    rdafolder = fullfile(workingfolder,subjects(crun).name);
    rdaamcc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_AMCC'),'^20.*\.img$');
    rdapgacc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_PGACC'),'^20.*\.img$');
    rda23c = spm_select('ExtFPList',fullfile(rdafolder,'MRS_23C'),'^20.*\.img$');
    rdapcc = spm_select('ExtFPList',fullfile(rdafolder,'MRS_PCC'),'^20.*\.img$');
    fprintf('%s\n',rdaamcc);
    fprintf('%s\n',rdapgacc);
    fprintf('%s\n',rda23c);
    fprintf('%s\n',rdapcc);
    
    
    t1folder = fullfile(workingfolder,subjects(crun).name,'T1/imcalc/');
    p1image = fullfile(t1folder,'p1enMPRAGE_corrected.nii');
    p2image = fullfile(t1folder,'p2enMPRAGE_corrected.nii');
    p3image = fullfile(t1folder,'p3enMPRAGE_corrected.nii');
    if exist(p1image) == 0
        %         p1image = fullfile(t1folder,'sp1nenMPRAGE.nii');
        %         p2image = fullfile(t1folder,'sp2nenMPRAGE.nii');
        %         p3image = fullfile(t1folder,'sp3nenMPRAGE.nii');
        break
    else
        fprintf('%s\n',p1image);
        fprintf('%s\n',p2image);
        fprintf('%s\n',p3image);
        
        p1HeaderInfo = spm_vol(p1image);
        p1Data = spm_read_vols(p1HeaderInfo);
        p2HeaderInfo = spm_vol(p2image);
        p2Data = spm_read_vols(p2HeaderInfo);
        p3HeaderInfo = spm_vol(p3image);
        p3Data = spm_read_vols(p3HeaderInfo);
        
        if size(rdaamcc,1) == 1
            rdaamccHeaderInfo = spm_vol(rdaamcc);
            rdaamccData = spm_read_vols(rdaamccHeaderInfo);
            rdaamccData(rdaamccData>0) = 1;
            temp = p1Data.* rdaamccData;
            gm_wm_csf(crun,1) = sum(temp(:));
            temp = p2Data.* rdaamccData;
            gm_wm_csf(crun,2) = sum(temp(:));
            temp = p3Data.* rdaamccData;
            gm_wm_csf(crun,3) = sum(temp(:));
            
            %         temp = (rdaamccData>0 & (p1Data>0));
            %         gm_wm_csf(crun,1) = numel(find(temp==1));
            %         temp = (rdaamccData>0 & (p2Data>0));
            %         gm_wm_csf(crun,2) = numel(find(temp==1));
            %         temp = (rdaamccData>0 & (p3Data>0));
            %         gm_wm_csf(crun,3) = numel(find(temp==1));
        else
            break
        end
        
        if size(rdapgacc,1) == 1
            rdapgaccHeaderInfo = spm_vol(rdapgacc);
            rdapgaccData = spm_read_vols(rdapgaccHeaderInfo);
            rdapgaccData(rdapgaccData>0) = 1;
            temp = p1Data.* rdapgaccData;
            gm_wm_csf(crun,4) = sum(temp(:));
            temp = p2Data.* rdapgaccData;
            gm_wm_csf(crun,5) = sum(temp(:));
            temp = p3Data.* rdapgaccData;
            gm_wm_csf(crun,6) = sum(temp(:));
            
            %         temp = (rdapgaccData>0 & (p1Data>0.2));
            %         gm_wm_csf(crun,4) = numel(find(temp==1));
            %         temp = (rdapgaccData>0 & (p2Data>0.2));
            %         gm_wm_csf(crun,5) = numel(find(temp==1));
            %         temp = (rdapgaccData>0 & (p3Data>0.2));
            %         gm_wm_csf(crun,6) = numel(find(temp==1));
        else
            break
        end
        
        if size(rda23c,1) == 1
            rda23cHeaderInfo = spm_vol(rda23c);
            rda23cData = spm_read_vols(rda23cHeaderInfo);
            rda23cData(rda23cData>0) = 1;
            temp = p1Data.* rda23cData;
            gm_wm_csf(crun,7) = sum(temp(:));
            temp = p2Data.* rda23cData;
            gm_wm_csf(crun,8) = sum(temp(:));
            temp = p3Data.* rda23cData;
            gm_wm_csf(crun,9) = sum(temp(:));
            
            %         temp = (rda23cData>0 & (p1Data>0));
            %         gm_wm_csf(crun,1) = numel(find(temp==1));
            %         temp = (rda23cData>0 & (p2Data>0));
            %         gm_wm_csf(crun,2) = numel(find(temp==1));
            %         temp = (rda23cData>0 & (p3Data>0));
            %         gm_wm_csf(crun,3) = numel(find(temp==1));
        else
            break
        end
        
        if size(rdapcc,1) == 1
            rdapccHeaderInfo = spm_vol(rdapcc);
            rdapccData = spm_read_vols(rdapccHeaderInfo);
            rdapccData(rdapccData>0) = 1;
            temp = p1Data.* rdapccData;
            gm_wm_csf(crun,10) = sum(temp(:));
            temp = p2Data.* rdapccData;
            gm_wm_csf(crun,11) = sum(temp(:));
            temp = p3Data.* rdapccData;
            gm_wm_csf(crun,12) = sum(temp(:));
            
            %         temp = (rdapccData>0 & (p1Data>0));
            %         gm_wm_csf(crun,1) = numel(find(temp==1));
            %         temp = (rdapccData>0 & (p2Data>0));
            %         gm_wm_csf(crun,2) = numel(find(temp==1));
            %         temp = (rdapccData>0 & (p3Data>0));
            %         gm_wm_csf(crun,3) = numel(find(temp==1));
        else
            break
        end
        
    end
end

amcc = gm_wm_csf(:,1:3);
pgacc = gm_wm_csf(:,4:6);
ratio_amcc= amcc(:,2)./sum(amcc,2);
ratio_pgacc = pgacc(:,2)./sum(pgacc,2);

gm_wm_csf(:,7) = ratio_amcc;
gm_wm_csf(:,8) = ratio_pgac;