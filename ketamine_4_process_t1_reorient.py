import sys, os, glob, fnmatch, shutil,commands
from nipype.interfaces.freesurfer import ParseDICOMDir
from nipype.interfaces.fsl import BET
from nipype.interfaces.dcm2nii import Dcm2nii
from nipype.interfaces.fsl import ImageMaths
from nipype.interfaces.ants import N4BiasFieldCorrection
from nipype.pipeline.engine import Node, MapNode, Workflow
import nipype.interfaces.spm as spm
import nipype.pipeline.engine as pe

src = "/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess"
subjs = os.listdir(src)
#subjs.remove("ac49")
subjs = ["ia45_2"]
subjs.sort()
#dst = src


for subj in subjs:
	src_subjfolder = os.path.join(src,subj)
	if not subj.startswith('.') and os.path.isdir(src_subjfolder):
		print subj		

		mrsfilter = ('%s/MRS*/*img'%(src_subjfolder)) 
		mrsfiles = glob.glob(mrsfilter)
		print mrsfiles

		tmp = glob.glob(os.path.join(src_subjfolder,'T1/imcalc/MPRAGE/convert_BET/convert_dicom/t_20*.nii.gz'))
		t1_mprage_t = os.path.abspath(tmp[0])

		tmp = glob.glob(os.path.join(src_subjfolder,'T1/imcalc/GE/convert_BET/convert_dicom/t_20*.nii.gz'))
		t1_ge_t = os.path.abspath(tmp[0])
		print t1_ge_t

		os.chdir(os.path.join(src_subjfolder,'T1/imcalc/'))

		enimg = 'MPRAGE.nii.gz'
		maths = ('fslmaths %s -div %s -uthr 1 -mul 1000 %s -odt int'%(t1_mprage_t,t1_ge_t,enimg))
		print maths
		os.system(maths)

		n4 = N4BiasFieldCorrection()
		n4.inputs.dimension = 3
		n4.inputs.input_image = enimg
		# n4.output_image = 'n4MPRAGE.nii.gz'
		# n4.inputs.bspline_fitting_distance = 300
		# n4.inputs.shrink_factor = 3
		# n4.inputs.n_iterations = [50,50,30,20]
		# n4.inputs.convergence_threshold = 1e-6
		n4.run()

		n4enimg = enimg.replace('.nii.gz','_corrected.nii.gz')
		chtype = ('/usr/local/fsl/bin/fslchfiletype NIFTI %s'%(n4enimg))
		print chtype
		os.system(chtype)

		mpragefile = os.path.join(src_subjfolder,'T1/imcalc/',n4enimg)

		print mpragefile

		coregister = spm.Coregister()
		coregister.inputs.jobtype = 'write'
		coregister.inputs.target = '/Volumes/Data_Meng_Canlab/mni_icbm152_nlin_sym_09b_nifti/mni_icbm152_nlin_sym_09b_Resliced/Resliced_mni_icbm152_t1_tal_nlin_sym_09b_hires.nii'
		coregister.inputs.source = mpragefile[0:-3]
		coregister.inputs.apply_to_files = mrsfiles
		coregister.run()

		segment = spm.Segment()
		segment.inputs.data = mpragefolder + '/renMPRAGE_corrected.nii'
		segment.gm_output_type = [False,False,True]
		segment.wm_output_type = [False,False,True]
		segment.csf_output_type = [False,False,True]
		segment.clean_masks = 'no'
		segment.gaussians_per_class = [2,2,2,4]
		segment.affine_regularization = 'mni'
		segment.warping_regularization = 1
		segment.warp_frequency_cutoff = 25
		segment.bias_regularization = 0.0001
		segment.bias_fwhm = 60
		segment.sampling_distance = 3
		segment.run() 

		seg = spm.VBMSegment()
		seg.inputs.tissues = '/Users/canlab06/Documents/Soft/matlab_addon/spm8/toolbox/Seg/TPM.nii'
		seg.inputs.dartel_template = '/Users/canlab06/Documents/Soft/matlab_addon/spm8/toolbox/vbm8/Template_1_IXI550_MNI152.nii'
		seg.inputs.bias_corrected_native = True
		seg.inputs.gm_native = True
		seg.inputs.wm_native = True
		seg.inputs.csf_native = True
		seg.inputs.pve_label_native = True
		seg.inputs.deformation_field = (True, False)
		seg.run() 



