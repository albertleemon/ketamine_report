import sys, os, glob, fnmatch, shutil,commands,gzip
# mnc_dir = "/Volumes/Data_Meng_Canlab/Weiqiang-mnc/"
# src_dir = "/Volumes/Data_Meng_Canlab/Weiqiang-preprocess/"

mnc_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-mnc/"
src_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess/"

if not os.path.exists(mnc_dir):
	os.mkdir(mnc_dir)

subjids = os.listdir(src_dir)
subjids.sort()
for subj in subjids:	
	if not subj.startswith('.') and not '.png' in subj and '_1_Messung' in subj:
		print subj
		subj_dir = os.path.join(src_dir,subj,'T1/imcalc') 
		if len(os.listdir(subj_dir))>0:	
			os.chdir(subj_dir)
			T1 = glob.glob("enMPRAGE_corrected.nii")
			mnc_name = 'ketamine_' + subj.replace('_','-') + '_t1.mnc'
			if not os.path.exists(mnc_name):
				nii2mnc = ("/opt/minc/bin/nii2mnc %s %s" %(T1[0],mnc_name))
				print nii2mnc
				os.system(nii2mnc)
				if not os.path.exists(os.path.join(mnc_dir,mnc_name)):
					cp = ("cp %s %s" %(mnc_name,mnc_dir))
					print cp
					os.system(cp)

