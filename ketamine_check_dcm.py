import sys, os, glob, fnmatch, shutil,commands,subprocess,re
src_dir = "/Volumes/HD/canlab/7T_ketamine/ketamin/"
subjids = os.listdir(src_dir)
subjids.sort()
#subjids = ['bh27']

info_txt = open('/Users/canlab06/Repository/py_repository/Ketamine_report/study_series_infos.txt','w')
#sys.stdout = myFile
for subjid in subjids:
	if not subjid.startswith('.') and os.path.isdir(os.path.join(src_dir,subjid)): 
		print subjid
		subj_src_dir = os.path.join(src_dir,subjid)
		messungs = ['1_Messung','2_Messung','3_Messung']
		for messung in messungs:
			print messung
			str1 = [''] * 29
			#subj_src_dir_messung = os.path.join(subj_src_dir,messung)
			dcm_sorted = os.path.join(subj_src_dir,messung,'dcm_sorted')
			for dcmpath, dcmdirs, dcmfiles in os.walk(dcm_sorted):
				if len(dcmdirs) == 0:
					#print dcmpath
					str1[0] = subjid
					str1[1] = messung
					basename = os.path.basename(dcmpath).lower()
					dirname = os.path.dirname(dcmpath).lower()
					if ("iso" in basename and "_mprage" in basename and '_nd' in basename): 	
						localfiles = glob.glob(os.path.join(dcmpath,"*"))
						#print dcmpath
						#print localfiles[0]	

						str1[2] = basename
						str1[3] = str(len(dcmfiles))

						name = ("dcmdump %s | grep '(0010,0010)' | sed -e 's/^(0010,0010).*\[\(.*\)\].*/\%d/'" %(localfiles[0],1))
						#print name
						# os.system(name)
						proc = subprocess.Popen(name, shell=True, stdout=subprocess.PIPE)
						subj_name = proc.communicate()[0]
						pattern = re.compile(r'\s+')
						subj_name = re.sub(pattern, '', subj_name)
						str1[26] = subj_name

						studytime = ("dcmdump %s | grep '(0008,0020)' | sed -e 's/^(0008,0020).*\[\(.*\)\].*/\%d/'" %(localfiles[0],1))
						#print studytime
						#os.system(studytime)
						proc = subprocess.Popen(studytime, shell=True, stdout=subprocess.PIPE)
						subj_studytime = proc.communicate()[0]
						pattern = re.compile(r'\s+')
						subj_studytime = re.sub(pattern, '', subj_studytime)
						str1[27] = subj_studytime

						scantime = ("dcmdump %s | grep '(0008,0031)' | sed -e 's/^(0008,0031).*\[\(.*\)\].*/\%d/'" %(localfiles[0],1))
						#print scantime
						#os.system(scantime)
						proc = subprocess.Popen(scantime, shell=True, stdout=subprocess.PIPE)
						subj_scantime = proc.communicate()[0]
						pattern = re.compile(r'\s+')
						subj_scantime = re.sub(pattern, '', subj_scantime)
						str1[28] = subj_scantime

						# TR = ("dcmdump %s | grep '(0018,0080)' | sed -e 's/^(0018,0080).*\[\(.*\)\].*/\%d/'" %(localfiles[0],1))
						# #print scantime
						# #os.system(scantime)
						# proc = subprocess.Popen(TR, shell=True, stdout=subprocess.PIPE)
						# subj_TR = proc.communicate()[0]
						# pattern = re.compile(r'\s+')
						# subj_TR = re.sub(pattern, '', subj_TR)
						# str1[29] = subj_TR

					
					elif ("iso" in basename and "_mprage" in basename and not '_nd' in basename): 	
						# print dcmpath					
						str1[4] = basename
						str1[5] = str(len(dcmfiles))

					elif ("iso" in basename and "_ge" in basename and '_nd' in basename): 	
						# print dcmpath					
						str1[6] = basename
						str1[7] = str(len(dcmfiles))

					elif ("iso" in basename and "_ge" in basename and not '_nd' in basename): 	
						# print dcmpath					
						str1[8] = basename
						str1[9] = str(len(dcmfiles))

					elif ("bold_trap" in basename and 'bold_trap' in dirname and not '_rr' in basename): 	
						# print dcmpath					
						str1[10] = 'rsfMRI_' + basename
						str1[11] = str(len(dcmfiles))

					elif ("mocos" in basename and 'bold_trap' in dirname and not '_rr' in basename): 	
						# print dcmpath					
						str1[12] = 'rsfMRI_' + basename
						str1[13] = str(len(dcmfiles))	

					# elif ("bold_trap" in basename and 'bold_trap' in dirname and '_rr' in basename): 	
					# 	# print dcmpath
					# 	str1[14] = 'rsfMRI_' + basename
					# 	str1[15] = str(len(dcmfiles))

					# elif ("mocos" in basename and 'bold_trap' in dirname and '_rr' in basename): 	
					# 	# print dcmpath					
					# 	str1[16] = 'rsfMRI_' + basename
					# 	str1[17] = str(len(dcmfiles))	

					elif ("bold_run" in basename and 'bold_run' in dirname and not '_rr' in basename): 	
						# print dcmpath					
						str1[18] = 'taskfMRI_' + basename
						str1[19] = str(len(dcmfiles))

					elif ("moco" in basename and 'bold_run' in dirname and not '_rr' in basename): 	
						# print dcmpath					
						str1[20] = 'taskfMRI_' + basename
						str1[21] = str(len(dcmfiles))	
											
					# elif ("bold_run" in basename and 'bold_run' in dirname and '_rr' in basename): 	
					# 	# print dcmpath					
					# 	str1[22] = 'taskfMRI_' + basename
					# 	str1[23] = str(len(dcmfiles))

					# elif ("moco" in basename and 'bold_run' in dirname and '_rr' in basename): 	
					# 	# print dcmpath					
					# 	str1[24] = 'taskfMRI_' + basename
					# 	str1[25] = str(len(dcmfiles))	


			print '\t'.join(str1) + '\n'
			info_txt.write('\t'.join(str1))		
	info_txt.write('\n')	
info_txt.close()						
