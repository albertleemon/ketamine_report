#!/bin/bash

source /home/fmri/Software/CIVET/Feb-14-2008/init.sh
#export FREESURFER_HOME=/home/fmri/Software/freesurfer
#source $FREESURFER_HOME/SetUpFreeSurfer.sh>/dev/null
civet=/home/fmri/Software/CIVET/Feb-14-2008
civet_result=/mnt/workspace/7T-CT-MRS/ketamine-CIVET-result
ct_mrs_mask_path=/mnt/workspace/7T-CT-MRS/ketamine-roi

# if [ ! -d "$ct_mrs_mask_path/orig_rois" ];then	
# 	mkdir $ct_mrs_mask_path/orig_rois
# fi 

if [ ! -d "$ct_mrs_mask_path/std_rois" ];then	
	mkdir $ct_mrs_mask_path/std_rois
fi

if [ ! -d "$ct_mrs_mask_path/stats" ];then	
	mkdir $ct_mrs_mask_path/stats
fi


for path in /mnt/workspace/7T-CT-MRS/ketamine-roi/*/*.mnc
	do
		echo path=$path
		subj=`echo "$path"|cut -d '/' -f 7`
		subj_civet_result=`echo $subj | sed 's/_/-/g'`


		echo subj=$subj
		roi=`echo "$path"|cut -d '/' -f 9`
		echo $roi >> $ct_mrs_mask_path/stats/stats.txt
		
		temp=`basename $roi .mnc`
		echo temp=$temp

	#   roi=`echo "$temp"|cut -d '-' -f 5`
	#   echo roi=$roi  
	#   index=${subj:0:1}
	#   echo index=$index
	#   name=${subj:1}
	#   echo name=$name  

		# roipath=$ct_mrs_mask_path/orig_rois/$subj
		# if [ ! -d "$roipath" ];then	
		# 	mkdir $roipath
		# fi

		roipath=`dirname $path`
		cd $roipath	
		# if [ ! -d "$roipath" ];then	
		# 	echo the work folder of current subject "$subj" does not exist	
		# 	echo $roipath	

		if [ ! -f "$temp.mnc" ];then
			# /home/fmri/Software/freesurfer/bin/mri_convert $path "$temp".mnc
			$civet/bin/mincresample -transform $civet_result/$subj_civet_result/transforms/linear/ketamine_"$subj_civet_result"_t1_tal.xfm -like $civet/share/ICBM/icbm_template_1.00mm.mnc $temp.mnc $ct_mrs_mask_path/std_rois/"$temp"_std.mnc
			$civet/bin/volume_object_evaluate -nearest_neighbour $ct_mrs_mask_path/std_rois/"$temp"_std.mnc $civet_result/$subj_civet_result/surfaces/ketamine_"$subj_civet_result"_mid_surface_left_81920.obj $ct_mrs_mask_path/stats/"$temp"_std_surface_left.txt
			$civet/bin/volume_object_evaluate -nearest_neighbour $ct_mrs_mask_path/std_rois/"$temp"_std.mnc $civet_result/$subj_civet_result/surfaces/ketamine_"$subj_civet_result"_mid_surface_right_81920.obj $ct_mrs_mask_path/stats/"$temp"_std_surface_right.txt
			$civet/bin/mincmath -mult $civet_result/$subj_civet_result/classify/ketamine_"$subj_civet_result"_classify.mnc $ct_mrs_mask_path/std_rois/"$temp"_std.mnc $ct_mrs_mask_path/stats/"$temp"_std_classify_ROI.mnc
			$civet/bin/mincstats -volume $ct_mrs_mask_path/stats/"$temp"_std_classify_ROI.mnc -binvalue 1 |grep -o ' [0-9]\+' >> $ct_mrs_mask_path/stats/stats.txt
			$civet/bin/mincstats -volume $ct_mrs_mask_path/stats/"$temp"_std_classify_ROI.mnc -binvalue 2 |grep -o ' [0-9]\+' >> $ct_mrs_mask_path/stats/stats.txt
			$civet/bin/mincstats -volume $ct_mrs_mask_path/stats/"$temp"_std_classify_ROI.mnc -binvalue 3 |grep -o ' [0-9]\+' >> $ct_mrs_mask_path/stats/stats.txt
		else
			echo "the roi file was finished"
		fi



	echo ----
done
exit 0