import sys, os, glob, fnmatch, shutil,commands,re
src_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-rda-result"
src_rda_txt = open("/Users/canlab06/Repository/Ketamine_report/ketamine_rda_infos_amcc.txt",'w')
#src_dir = "/Volumes/Data_Meng_Canlab/Weiqiang-rda-result"
subjids = os.listdir(src_dir)
subjids.sort()
#subjids = ['we81_1_Messung_MRS_AMCC','we81_2_MessungMRS_AMCC','we81_3_MessungMRS_AMCC','wx52_1_MessungMRS_AMCC','wx52_2_Messung','wx52_3_Messung']
print subjids
for subjid in subjids:
	if not subjid.startswith('.'):
		subjpath = os.path.join(src_dir,subjid)
		os.chdir(subjpath)
		if "AMCC" in subjpath:
			subjfiles = os.listdir(subjpath)
			##print subjid
			for subjfile in subjfiles:
				##print rdafile
				if 'rda' in subjfile and not '_h2o' in subjfile:
					str1 = [''] * 13

					with open(subjfile) as f:
						for line in f:
							line = line.replace('\n','')
							line = line.replace('\r','')
							##print line

							if 'PatientName: ' in line:
								name = line.replace('PatientName: ','')
								##print name
								str1[0] = name
							elif 'SeriesDate: ' in line:
								date = line.replace('SeriesDate: ','')
								##print date
								str1[1] = date
							elif 'SeriesTime: ' in line:
								Time =  line.replace('SeriesTime: ','')
								##print Time
								str1[2] = Time
							elif 'SeriesDescription: ' in line:
								Serie = line.replace('SeriesDescription: ','')
								##print Serie
								str1[3] = Serie
							elif 'TR: ' in line:
								TR = line.replace('TR: ','')
								##print TR
								str1[4] = TR
							elif 'TE: ' in line:
								TE = line.replace('TE: ','')
								##print TE
								str1[5] = TE
							elif 'NumberOfAverages' in line:
								NS = line.replace('NumberOfAverages: ','')
								##print NS
								str1[6] = NS
							elif 'FoVHeight: ' in line:
								Height = line.replace('FoVHeight: ','')
								##print Height
								str1[7] = Height
							elif 'FoVWidth: ' in line:
								Width = line.replace('FoVWidth: ','')
								##print Width
								str1[8] = Width
							elif 'FoV3D: ' in line:
								Length = line.replace('FoV3D: ','')
								##print Length
								str1[9] = Length
							elif 'InstanceComments: ' in line:
								Comments = line.replace('InstanceComments: ','')
								##print Comments
								str1[10] = Comments
						print '\t'.join(str1)
						src_rda_txt.write('\t'.join(str1))	
						#src_rda_txt.write(subjid + '\t' + subjfile + '\t' + name + '\t' + date + '\t' +Time  + '\t' +Serie  + '\t' +TR  + '\t' + TE  + '\t' + NS  + '\t' +Height + '\t' +Width + '\t' + Length + '\t' + Comments)
				elif 'table' in subjfile:
					str2 = [''] * 28
					with open(subjfile) as g:
						for line in g:
							line = line.replace('\n','')
							line = line.replace('\r','')
							##print line
							str2[0]  = subjid
							if 'FWHM' in line:
								line_parts = line.split(' ')
								fwhm = line_parts[4]
								snr = line_parts[12]
								str2[1] = fwhm
								str2[2] = snr
								#print "fwhm:" + fwhm
								#print "snr:" + snr
							elif 'HZPPPM' in line:
								line_parts = line.split(' ')
								hzpppm = line_parts[1].replace('HZPPPM=','')
								#print "hzpppm:" + hzpppm
								str2[3] = hzpppm
							elif 'WCONC' in line:
								line_parts = line.split(' ')
								wconc = line_parts[1].replace('WCONC=','')
								#print "wconc:" + wconc
								str2[4] = wconc
							elif 'ATTMET' in line:
								line_parts = line.split(' ')
								attmet = line_parts[1].replace('ATTMET=','')
								#print "attmet:" + attmet
								str2[5] = attmet
							elif 'ATTH2O' in line:
								line_parts = line.split(' ')
								atth2o = line_parts[1].replace('ATTH2O=','')
								#print "atth2o:"+ atth2o
								str2[6] = atth2o
							elif re.match('(.*) Cr (.*)',line):
								newline = re.sub(' +',' ',line)
								line_parts = newline.split(' ')
								Cr_conc = line_parts[1]
								Cr_sd = line_parts[2]
								Cr_cr_pcr = line_parts[3]
								#print "Cr_conc:" + Cr_conc
								#print "Cr_sd:" + Cr_sd
								#print "Cr_cr_pcr:" + Cr_cr_pcr 
								str2[7] = Cr_conc
								str2[8] = Cr_sd
								str2[9] = Cr_cr_pcr
							elif re.match('(.*) Glu (.*)',line):
								newline = re.sub(' +',' ',line)
								line_parts = newline.split(' ')
								glu_conc = line_parts[1]
								glu_sd = line_parts[2]
								glu_cr_pcr = line_parts[3]
								#print "glu_conc:" + glu_conc
								#print "glu_sd:" + glu_sd
								#print "glu_cr_pcr:" + glu_cr_pcr 
								str2[10] = glu_conc
								str2[11] = glu_sd
								str2[12] = glu_cr_pcr
							elif re.match('(.*) Gln (.*)',line):
								newline = re.sub(' +',' ',line)
								line_parts = newline.split(' ')
								gln_conc = line_parts[1]
								gln_sd = line_parts[2]
								gln_cr_pcr = line_parts[3]
								#print "gln_conc:" + gln_conc
								#print "gln_sd:" + gln_sd
								#print "gln_cr_pcr:" + gln_cr_pcr 
								str2[13] = gln_conc
								str2[14] = gln_sd
								str2[15] = gln_cr_pcr
							elif re.match('(.*) Glu\+Gln (.*)',line): 
								newline = re.sub(' +',' ',line)
								line_parts = newline.split(' ')
								glx_conc = line_parts[1]
								glx_sd = line_parts[2]
								glx_cr_pcr = line_parts[3]
								#print "glx_conc:" + glx_conc
								#print "glx_sd:" + glx_sd
								#print "glx_cr_pcr:" + glx_cr_pcr 
								str2[16] = glx_conc
								str2[17] = glx_sd
								str2[18] = glx_cr_pcr
							elif re.match('(.*) GABA (.*)',line): 
								newline = re.sub(' +',' ',line)
								line_parts = newline.split(' ')
								gaba_conc = line_parts[1]
								gaba_sd = line_parts[2]
								gaba_cr_pcr = line_parts[3]
								#print "gaba_conc:" + gaba_conc
								#print "gaba_sd:" + gaba_sd
								#print "gaba_cr_pcr:" + gaba_cr_pcr 
								str2[19] = gaba_conc
								str2[20] = gaba_sd
								str2[21] = gaba_cr_pcr
							elif re.match('(.*) NAA (.*)',line):
								newline = re.sub(' +',' ',line)
								line_parts = newline.split(' ')
								naa_conc = line_parts[1]
								naa_sd = line_parts[2]
								naa_cr_pcr = line_parts[3]
								#print "naa_conc:" + naa_conc
								#print "naa_sd:" + naa_sd
								#print "naa_cr_pcr:" + naa_cr_pcr 
								str2[22] = naa_conc
								str2[23] = naa_sd
								str2[24] = naa_cr_pcr
						print '\t'.join(str2) + '\n'
						src_rda_txt.write('\t'.join(str2)+'\n')	
						###src_rda_txt.write(subjid + '\t' + snr + '\t' + fwhm + '\t' + hzpppm + '\t' + wconc  + '\t' + attmet  + '\t' + atth2o  + '\t' + Cr_conc  + '\t' + Cr_sd  + '\t' + Cr_cr_pcr + '\t' + glu_conc + '\t' + glu_sd + '\t' + glu_cr_pcr   + '\t' + gln_conc + '\t' + gln_sd + '\t' + gln_cr_pcr  + '\t' + glx_conc + '\t' + glx_sd + '\t' + glx_cr_pcr  + '\t' + glx_conc + '\t' + glx_sd + '\t' + glx_cr_pcr  + '\t' + gaba_conc + '\t' + gaba_sd + '\t' + gaba_cr_pcr + '\t' + naa_conc + '\t' + naa_sd + '\t' + naa_cr_pcr + '\n')
src_rda_txt.close()


