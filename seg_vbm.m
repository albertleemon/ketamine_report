% List of open inputs
% VBM8: Estimate & Write: Volumes - cfg_files
nrun = X; % enter the number of runs here
jobfile = {'/Volumes/Data_Meng_Canlab/Weiqiang-preprocess/ia45_2/T1/imcalc/seg_vbm_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(1, nrun);
for crun = 1:nrun
    inputs{1, crun} = MATLAB_CODE_TO_FILL_INPUT; % VBM8: Estimate & Write: Volumes - cfg_files
end
spm('defaults', 'FMRI');
spm_jobman('serial', jobs, '', inputs{:});
