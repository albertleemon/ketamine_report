workingfolder = '/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess';
subjects = dir(workingfolder);
ind2rm = false(1,numel(subjects));
for iter = 1: numel(subjects)
    if (isempty(strfind(subjects(iter).name,'ww54'))) & numel((strfind(subjects(iter).name,'_1_Messung'))) == 1 & exist(fullfile(workingfolder,subjects(iter).name,'T1/imcalc/p1enMPRAGE_corrected.nii')) == 0
        ind2rm(iter) = true;
    end
end
subjects = subjects(ind2rm);

subjnumber = numel(subjects);
% List of open inputs
% VBM8: Estimate & Write: Volumes - cfg_files
nrun = subjnumber; % enter the number of runs here
jobfile = {'/Volumes/Data_Meng_Canlab/Weiqiang-preprocess/ia45_2/T1/imcalc/seg_vbm_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(1, nrun);
for crun = 1:nrun
    t1folder = fullfile(workingfolder,subjects(crun).name,'T1/imcalc/');      
    t1image = fullfile(t1folder,'enMPRAGE_corrected.nii');
    inputs{1, crun} = cellstr(t1image); % VBM8: Estimate & Write: Volumes - cfg_files  
end
spm('defaults', 'FMRI');
spm_jobman('serial', jobs, '', inputs{:});

for crun = 1: nrun
    t1folder = fullfile(workingfolder,subjects(crun).name,'T1/imcalc/');  
    c1image = fullfile(t1folder,'p1enMPRAGE_corrected.nii');
    c1HeaderInfo = spm_vol(c1image);
    c1Data = spm_read_vols(c1HeaderInfo);
    midimg = c1Data(:,:,size(c1Data,3)/2);
%     imshow(midimg,[]);
    midimg_verify = [workingfolder,filesep,subjects(crun).name,'_','midimg_verify.png']
    imwrite(midimg,midimg_verify)
end